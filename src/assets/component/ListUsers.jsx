import React from 'react';
import Modal from 'react-modal';

const ListUsers = ({ users, onEditUser }) => {
    return (
        <div className='container-users'>
            <h2>Liste des Utilisateurs</h2>
            <ul className='listUsers'>
                {users.map((user, index) => (
                    <li key={index} className='infosUser'>
                        <div className='infosImg'>
                            <img
                                src={
                                    user.gender === 'male'
                                        ? '/male.png'
                                        : user.gender === 'female'
                                            ? '/female.png'
                                            : '/default.png'
                                }
                                alt={`Image pour ${user.gender}`}
                            />
                        </div>
                        <div className='infosContent'>
                            <p>Nom: {user.lastName}</p> <p>Prénom: {user.firstName}</p> <p>Âge: {user.age}</p> <p>Genre: {user.gender}</p>
                            <button onClick={() => onEditUser(user)}>Modifier</button>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default ListUsers;
