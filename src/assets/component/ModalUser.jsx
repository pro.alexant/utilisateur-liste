import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';

const ModalAddUsers = ({ isOpen, onRequestClose, formData, handleChange, handleSubmit, mode, onEditSubmit, onDeleteUser }) => {

    return (
        <Modal isOpen={isOpen} onRequestClose={onRequestClose}>
            <div className="modal">
                {mode === 'add' ? (
                    <>
                        <h2>Ajouter un Utilisateur</h2>
                        <form onSubmit={handleSubmit}>
                            <label>Nom :</label>
                            <input type="text" name="lastName" value={formData.lastName} onChange={handleChange} />

                            <label>Prénom :</label>
                            <input type="text" name="firstName" value={formData.firstName} onChange={handleChange} />

                            <label>Âge :</label>
                            <input type="number" name="age" value={formData.age} onChange={handleChange} />

                            <label>Genre :</label>
                            <select name="gender" value={formData.gender} onChange={handleChange}>
                                <option value="">Sélectionner</option>
                                <option value="male">Homme</option>
                                <option value="female">Femme</option>
                            </select>

                            <button type="submit">Ajouter</button>
                        </form>
                    </>) : (
                    <>
                        <h2>Modifier un Utilisateur</h2>
                        <form onSubmit={onEditSubmit}>
                            <label>Nom :</label>
                            <input type="text" name="lastName" value={formData.lastName} onChange={handleChange} />

                            <label>Prénom :</label>
                            <input type="text" name="firstName" value={formData.firstName} onChange={handleChange} />

                            <label>Âge :</label>
                            <input type="number" name="age" value={formData.age} onChange={handleChange} />

                            <label>Genre :</label>
                            <select name="gender" value={formData.gender} onChange={handleChange}>
                                <option value="">Sélectionner</option>
                                <option value="male">Homme</option>
                                <option value="female">Femme</option>
                            </select>

                            <button type="submit" >Modifier</button>
                            <button type="button" onClick={onDeleteUser} >Supprimer</button>
                        </form>
                    </>
                )}

            </div>
        </Modal>
    );
};

export default ModalAddUsers;
