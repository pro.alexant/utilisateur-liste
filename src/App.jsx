import React, { useState } from 'react';
import Modal from 'react-modal';
import './reset.css'
import './App.scss';
import ModalUser from './assets/component/ModalUser';
import ListUsers from './assets/component/ListUsers';

const App = () => {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [isAddMode, setIsAddMode] = useState(false);
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    age: '',
    gender: '',
  });

  const openModal = () => {
    setIsAddMode(true);
    setModalIsOpen(true);
  };

  const closeModal = () => {
    setModalIsOpen(false);
  };

  const submitModal = (formData) => {
    setUsers((prevUsers) => [...prevUsers, formData]);
    closeModal();
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const newUser = { ...formData };
    submitModal(newUser);
    setFormData({
      firstName: '',
      lastName: '',
      age: '',
      gender: '',
    });
    closeModal();
  };

  const handleEditSubmit = (e) => {
    e.preventDefault();
    const updatedUsers = [...users];
    const userIndex = updatedUsers.findIndex(user => user.id === selectedUser.id);
    if (userIndex !== -1) {
      updatedUsers[userIndex] = { ...selectedUser, ...formData };
      setUsers(updatedUsers);
    }
    closeModal();
  };

  const handleEditUser = (user) => {
    setSelectedUser(user);
    setIsAddMode(false);
    setModalIsOpen(true);
  };

  const handleDeleteUser = () => {
    if (selectedUser) {
      const updatedUsers = users.filter(user => user.id !== selectedUser.id);
      setUsers(updatedUsers);
      setSelectedUser(null);
      closeModal();
    }
  };

  return (
    <div className='container'>
      <ListUsers users={users} onEditUser={handleEditUser} />
      <button className='btn btnAddUser' onClick={openModal}>Ajouter des Utilisateurs</button>
      <ModalUser
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        onSubmit={submitModal}
        formData={formData}
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        mode={isAddMode ? 'add' : 'edit'}
        onEditSubmit={handleEditSubmit}
        onDeleteUser={handleDeleteUser}
      />
    </div>
  );
};

export default App;
